package uk.ac.mimas.names.exampledatahandler;
import uk.ac.mimas.names.disambiguator.NamesDisambiguator;
import uk.ac.mimas.names.disambiguator.types.*;
import uk.ac.mimas.names.disambiguator.util.PersonNameParser;
import uk.ac.mimas.names.databasemanager.DatabaseManager;
import uk.ac.mimas.names.databasemanager.Query;
import uk.ac.mimas.names.databasemanager.Response;
import uk.ac.mimas.names.databasemanager.types.NamesRecord;
import org.apache.log4j.Logger;
import org.apache.log4j.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.io.FileOutputStream;
import java.util.Properties;
import java.io.FileInputStream;
/**
 * 
 * @author danielneedham
 *
 */
public class ExampleDataHandlerTwo {
	private static final Logger logger = Logger.getLogger(ExampleDataHandlerTwo.class.getName());
	public static ArrayList<ExampleRecord> dataSource;
	public static int current = 0;
	public static int batchMax = 50;
	public static int procMax = 1000;
	public static double threshold = 50.0;
	public static String driver;
	public static String database;
	public static String username;
	public static String password;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		logger.setLevel(Level.INFO);
		logger.info("Starting ExampleDataAnalyser.");
		try{
			loadProperties();
			initaliseDatasource();
			List<ExampleRecord> batch = new ArrayList<ExampleRecord>();
			// 1. Grab a batch of local records
			// here we will assume there is only a few and they
			// can be held in memory, however we could 
			// create a temporary database table to hold them 
			// in if necessary
			while(current <= procMax){
				// fill source with a new batch
				batch.clear();
				batch = getNextBatch();
				if(batch.size() == 0)
					break;
				
				// 2. Transform local records into Names records
				ArrayList<NormalisedRecord> transformedRecords = new ArrayList<NormalisedRecord>();
				for(ExampleRecord e : batch){
					transformedRecords.add(transformRecord(e));
				}
				batch.clear();
			
				
				// 3. Use NamesDisambiguator to deduplicate collection
				// We pass it a normalised batch and it returns the same 
				// batch with matchedness scores for each comparison record
				NamesDisambiguator disambiguator = new NamesDisambiguator();
				disambiguator.matchCollection(transformedRecords);
			
				// 4. Merge the source collection at the required threshold ; in this case 50%
				transformedRecords = disambiguator.mergeCollection(transformedRecords, threshold);
				
				// 5. Create a new Names DB manager
				DatabaseManager dm = new DatabaseManager(driver,database,username,password);
				dm.connect();

				// 6. Iterate through transformed records and attempt to
				// find matches in names DB
				ArrayList<NormalisedRecord> potentialMatches = new ArrayList<NormalisedRecord>();
				ArrayList<NamesRecord> matched = new ArrayList<NamesRecord>();
				Query nameQuery;
				Response response;
				for(NormalisedRecord sourceRecord : transformedRecords){
						sourceRecord.getMatchResults().clear();  				// clear any previous match results
						nameQuery = new Query();
						for(NormalisedName normalisedName : sourceRecord.getNormalisedNames()){
							if(!normalisedName.equals(sourceRecord.getNormalisedNames().get(0)))
								nameQuery.setNames(nameQuery.getNames() + " OR ");
							nameQuery.setNames(nameQuery.getNames() + "\""+normalisedName.getName()+"*\"");
						}
						logger.info("query names: `" + nameQuery.getNames());
						response = dm.find(nameQuery);
						if(response.getStatus() == Response.OK
							&& response.getResults().getRecords().size() > 0){
								potentialMatches.add(sourceRecord);
								potentialMatches.addAll(response.getResults().getRecords());
								disambiguator.matchCollection(potentialMatches);
								NormalisedRecord bestMatch = sourceRecord.getBestMatch();
								if(sourceRecord.getMatchResults().containsKey(bestMatch) 
									&& bestMatch.getMatchResults().get(sourceRecord) >= threshold){
										bestMatch.mergeWith(sourceRecord, bestMatch.getMatchResults().get(sourceRecord));
										matched.add((NamesRecord)bestMatch);

								// It would be possible to go on to repeat matching against
							    // the other potential matches here against the merged
								// best match
								}
								else{
							    	matched.add(new NamesRecord(sourceRecord));							// no matches found : convert source record to names record and add
							    }
						}
					    else{
					    	matched.add(new NamesRecord(sourceRecord));							// no matches found : convert source record to names record and add
					    }
				}


				// 7. update database, adding where no matches were found or otherwise
				// adding meta data to existing records where a match was found.

				dm.addUpdate(matched);


				// Close dm connection
				dm.close();

				current += batchMax;
				batchMax++;
			}
		}
		catch(Exception e){
			logger.error(e.toString());
			e.printStackTrace();
		}
		
		logger.info("Finished ExampleDataAnalyser.");
	}
	
	public static void loadProperties(){
		Properties prop = new Properties();
		try{
			prop.load(new FileInputStream("config.properties"));
 			driver = prop.getProperty("driver");
            database = prop.getProperty("database");
            username = prop.getProperty("username");
            password = prop.getProperty("password");
 
		}
		catch(Exception e){
			logger.error("Error loading properties " + e.toString() );
			System.exit(1);
		}
	}
	/**
	 * We used dataSource to simulate an external store of data. This could be a database, xml file, Application API etc..
	 * This method fills dataSource with example records.
	 */
	public static void initaliseDatasource(){
		dataSource = new ArrayList<ExampleRecord>();
		ExampleRecord e = new ExampleRecord();
		e.setInternalRecordID("#001");
		e.setGivenNames("Montgomery");
		e.setFamilyNames("Burns");
		e.setArticle("Accountancy in the Middle Ages.");
		e.setFieldOfInterest("Money");
		e.setInstitutionalAffiliation("Springfield University");
		dataSource.add(e);	
		e = new ExampleRecord();
		e.setInternalRecordID("#002");
		e.setGivenNames("Homer J.");
		e.setFamilyNames("Simpson");
		e.setFieldOfInterest("Donuts");
		e.setArticle("Nuclear Physics and thermo reactors.");
		e.setInstitutionalAffiliation("Springfield University");
		dataSource.add(e);	
		e = new ExampleRecord();
		e.setInternalRecordID("#003");
		e.setGivenNames("Bart");
		e.setFamilyNames("Simpson");
		e.setArticle("Eating shorts.");
		e.setFieldOfInterest("Skateboards");
		e.setInstitutionalAffiliation("Springfield University");
		dataSource.add(e);

		e = new ExampleRecord();
		e.setInternalRecordID("#004");
		e.setGivenNames("H. J.");
		e.setFamilyNames("Simpson");
		e.setArticle("Nuclear Physics, splitting the atom and thermo nuclear reactions.");
		e.setFieldOfInterest("Donuts");
		e.setInstitutionalAffiliation("Springfield University");
		dataSource.add(e);	
		
		e = new ExampleRecord();
		e.setInternalRecordID("#005");
		e.setGivenNames("B.");
		e.setFamilyNames("Simpson");
		e.setArticle("Short Eating.");
		e.setFieldOfInterest("Skateboards");
		e.setInstitutionalAffiliation("Springfield University");
		dataSource.add(e);	
		
		e = new ExampleRecord();
		e.setInternalRecordID("#006");
		e.setGivenNames("Homer");
		e.setFamilyNames("Simpson");
		e.setFieldOfInterest("Donuts");
		e.setArticle("Thermo reactors and physics, with some atoms included.");
		e.setInstitutionalAffiliation("Springfield University");
		dataSource.add(e);	
	}
	
	/**
	 * Returns a view of a portion of the data source arraylist.
	 * 
	 */
	
	public static List<ExampleRecord> getNextBatch(){
		int from = 0;
		int to = dataSource.size();
		
		from = current < 0 ? from : current;
		from = from < dataSource.size() ? from : dataSource.size();
		
		to = current + batchMax;
		to = to < 0 ? 0 : to;
		to = to < dataSource.size() ? to : dataSource.size();
		
		return dataSource.subList(from, to);
	}

	/**
	 * Finds an example record by its internal identifier
	 *
	 */
	public static ExampleRecord findByID(String identifier){
		for(ExampleRecord record : dataSource){
			if(record.getInternalRecordID().equalsIgnoreCase(identifier))
				return record;
		}
		return null;
	}


	/**
	 * Transforms an example record into a normalised record
	 * @param exampleRecord
	 * @return
	 */
	public static NormalisedRecord transformRecord(ExampleRecord exampleRecord){
		NormalisedRecord transformedRecord = new NormalisedRecord();
		transformedRecord.addNormalisedName(new NormalisedName(exampleRecord.getFamilyNames() + ", " + exampleRecord.getGivenNames()));
		PersonNameParser nameParser = new PersonNameParser();
		nameParser.parse(exampleRecord.getFamilyNames() + ", " + exampleRecord.getGivenNames());
		transformedRecord.getNormalisedTitles().addAll(nameParser.getTitles());
		transformedRecord.getNormalisedNames().get(0).getFamilyNames().addAll(nameParser.getFamilyNameComponents());
		transformedRecord.getNormalisedNames().get(0).getGivenNames().addAll(nameParser.getGivenNameComponents());
		transformedRecord.getNormalisedFieldsOfActivity().add(new NormalisedFieldOfActivity(exampleRecord.getFieldOfInterest()));
		transformedRecord.getNormalisedAffiliations().add(new NormalisedAffiliation(exampleRecord.getInstitutionalAffiliation()));
		NormalisedResultPublication article = new NormalisedResultPublication();
		article.setTitle(exampleRecord.getArticle());
		transformedRecord.addNormalisedResultPublication(article);
		NormalisedIdentifier identifier = new NormalisedIdentifier();
		identifier.setIdentifier(exampleRecord.getInternalRecordID());
		identifier.setBasisFor("EXAMPLE_SOURCE");
		transformedRecord.addNormalisedIdentifier(identifier);

		return transformedRecord;
	}

}
